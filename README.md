# Talent Sprint

- Ponto positivo
    Eu acredito que o ponto positivo da implementação são a movimentação do bot, os disparos e o range alto de detecção de inimigos.

- Ponto negativo
    As vezes por errar muitos disparos o bot acaba ficando sem energia e consequentemente para de atirar.

- O que mais gostei
    Gostei de aprender um pouco mais sobre C#, perceber as similaridades entre o c# e o java foi o mais facilitou a execução do desafio.

- Maior dificuldade
    Pensar em coisas legais para implementar no bot