﻿using System;
using Robocode;
using System.Drawing;

namespace VRO
{
    static class Constants
    {
        public const double baseMovimentacao = 180;    
        public const double rangeRadar = 1000;   
        public const double baseVolta = Math.PI / 2;    
        public const double baseForcaDisparo = 20;
        public const double infinitoPositivo = double.PositiveInfinity;
    }
    public class Veronica : AdvancedRobot
    {
        static double movimento;
        static String ultimoAlvo;
        static double ultimaDistancia;
        public override void Run()
        {
            SetColors(Color.Black, Color.White, Color.Red);
            IsAdjustGunForRobotTurn = true;
            InicializaValorBaseParaMovimento();
            OnRobotDeath(null);
            TurnRadarRight(Constants.infinitoPositivo);
        }
        private static void InicializaValorBaseParaMovimento()
        {
            movimento = Constants.infinitoPositivo;
        }
        public override void OnHitWall(HitWallEvent e)
        {
            if (Math.Abs(movimento) > Constants.baseMovimentacao)
            {
                movimento = Constants.baseMovimentacao;
            }
        }
        public override void OnRobotDeath(RobotDeathEvent e)
        {
            ultimaDistancia = Constants.infinitoPositivo;
        }
        public override void OnScannedRobot(ScannedRobotEvent e)
        {
            double rolamentoAbsoluto = e.Distance;

            if (DistanceRemaining == 0)
            {
                SetAhead(movimento = -movimento);
                SetTurnRightRadians(Constants.baseVolta);
            }

            if (ultimaDistancia > rolamentoAbsoluto)
            {
                ultimaDistancia = rolamentoAbsoluto;
                ultimoAlvo = e.Name;
            }

            if (ultimoAlvo == e.Name)
            {
                if (GunHeat < 1 && rolamentoAbsoluto < Constants.rangeRadar)
                {
                    if (GunHeat == GunTurnRemaining)
                    {
                        SetFireBullet(CalculaDisparo(rolamentoAbsoluto));
                        OnRobotDeath(null);
                    }
                    SetTurnRadarLeft(RadarTurnRemaining);
                }
                rolamentoAbsoluto = e.BearingRadians + HeadingRadians;
                SetTurnGunRightRadians(CalculaGiroDaArmaParaDireita(rolamentoAbsoluto, e));
            }
        }
        private double CalculaDisparo(double rolamentoAbsoluto)
        {
            return Energy * Constants.baseForcaDisparo / rolamentoAbsoluto;
        }
        private double CalculaGiroDaArmaParaDireita(double rolamentoAbsoluto, ScannedRobotEvent e)
        {
            return Math.Asin(Math.Sin(rolamentoAbsoluto - GunHeadingRadians +
                    (1 - e.Distance / 500) *
                    Math.Asin(e.Velocity / 11) * Math.Sin(e.HeadingRadians - rolamentoAbsoluto)));
        }
    }
}
